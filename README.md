Bankers
========

A Frankfurt Bankers Style Adaption of the icon QBasis Game *Gorillas*


####credit
JavaScript Port <http://michaeldeol.github.com/gorillas-js/>

Original QBasic Source - <http://hem.passagen.se/hedsen/prg/games/gorilla.bas>

Python Port - <http://inventwithpython.com/gorilla.py>

CoffeeScript Port - <https://github.com/despo/gorillas>

![alt](CoverImage.png)